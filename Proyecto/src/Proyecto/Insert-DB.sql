-- Insert into PEDIDOS
insert into PEDIDOS values(1,'Paquete',3,1,'SEUR',1,'NO');
insert into PEDIDOS values(2,'Paquete',3,1,'Fedex',2,'SI');
insert into PEDIDOS values(3,'Sobre',3,1,'Nosotros',3,'SI');
insert into PEDIDOS values(4,'Paler',3,1,'Nosotros',4,'NO');
insert into PEDIDOS values(5,'Sobre',3,1,'DHL',5,'NO');

-- Insert into Clientes

insert into clientes values(39911030,'Maria','Camacho Sanchez',43201,1,1,'Santana 5','mcam@hotmail.es',122587458,5,'Paypal');
insert into clientes values(39911031,'Marta','Montoro Bordera',43202,2,2,'Pont 45','martita_01@hotmail.com',613258741,1,'Efectivo');
insert into clientes values(39911033,'Oscar','Duran Lleida',43203,3,3,'Via Augusta 15','osky@gmail.com',321456890,2,'Visa');
insert into clientes values(39911034,'Adri�','Camacho Fuentes',43204,4,4,'Europa 6','adri_1992@hotmail.com',63254170,6,'Paypal');
insert into clientes values(39911035,'Lluis','Quintana Fernandez',43205,5,5,'Jesus 65','llquintana@yahoo.com',362514908,3,'Paypal');

-- insert into vehiculos

insert into vehiculos values('2926HHJ','Furgo');
insert into vehiculos values('3254DBT','Furgo');
insert into vehiculos values('3258BCD','Furgo');
insert into vehiculos values('3658HHH','Camion');
insert into vehiculos values('3695CWG','Camion');

-- insert into trabajadores

insert into trabajadores values(23659874563210,'Juan','Cano',39911085,'Astorga 1',698523014,'jcano@gmail.com',43200,'SI',1500,'3658HHH',1);
insert into trabajadores values(23659874563587,'Anna','Sanchez',39911080,'Marina 22',698523011,'annass@gmail.com',43201,'SI',1300,'2926HHJ',1);
insert into trabajadores values(23656325463210,'Sheila','Quintana',39911081,'Jovellanos 20',698523012,'sq1997@hotmail.com',43201,'NO',1350,'3254DBT',1);
insert into trabajadores values(23600021463210,'Marc','Casillas',39911082,'Sama 45',698523013,'marc_el_grande@gmail.com',43205,'NO',1400,'3695CWG',1);
insert into trabajadores values(66698774563210,'Gerard','Nazari',39911083,'Rambla nova 34',698523010,'gerry1989@yahoo.com',43205,'NO',1300,'3258BCD',1);

-- insert into envios

insert into envios values(1,1,1,23659874563210,'Reus',15);
insert into envios values(2,2,2,23659874563587,'Tarragona',5.2);
insert into envios values(3,3,3,23656325463210,'Valls',14);
insert into envios values(4,4,4,23600021463210,'Barcelona',1);
insert into envios values(5,5,5,66698774563210,'Reus',20);

-- insert into almacen

insert into almacen values(1,'Reus',1);
insert into almacen values(2,'Tarragona',2);
insert into almacen values(3,'Reus',3);
insert into almacen values(4,'Reus',4);
insert into almacen values(5,'Barcelona',5);

-- insert into proveedores

insert into proveedores values(1,39911070,'Transportes S.A',1,43200,'Pol.IND.Reus 20','info@transsa.com',977320521,1,5,'VIsa');
insert into proveedores values(2,39911071,'SEUR',2,43200,'Carrer Salou','info@seur.com',977325285,2,2,'VIsa');
insert into proveedores values(3,39911072,'DHL',3,43206,'Pol.IND.Reus 10','info@dhl.es',9773124587,3,4,'PayPal');
insert into proveedores values(4,39911073,'Gruas y Camiones S.A',4,43206,'Pol.IND.Reus.Sud 5','info@gya',977987452,4,5,'Efectivo');
insert into proveedores values(5,39911074,'Guillermat trans',5,43206,'La Sardaneta 21','contacte@guillermatt.cat',977312765,4,1,'VIsa');